﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("booksbycategory")]
    public class BookByCategory
    {
        [Key]
        [Column(Order = 1)]
        public int BookId { get; private set; }

        [Key]
        [Column(Order = 2)]
        public int CategoryId { get; private set; }

        private BookByCategory() { }

        public BookByCategory(int bookId, int categoryId)
        {
            BookId = bookId;
            CategoryId = categoryId;
        }
    }
}
