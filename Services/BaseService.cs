﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Services
{
    public class BaseService
    {
        public MySqlConnection GetConnetion()
        {
            return new MySqlConnection(ConfigurationManager.ConnectionStrings["library"].ConnectionString);
        }
    }
}
